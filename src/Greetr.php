<?php

namespace Hexxondiv\Greetr;

class Greetr
{
    public function greet($sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}
